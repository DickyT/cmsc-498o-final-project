import json

data = []

for month in range(1, 13):
    with open('raw_data/airbnb-nyc-2019-%d.json' % month, encoding='utf8') as fp:
        data.append({
            'time': '2019-%d' % month,
            'data': json.load(fp)
        })

with open('raw_data/airbnb-nyc-all.json', 'w', encoding='utf8') as fp:
    json.dump(data, fp, indent=4)
