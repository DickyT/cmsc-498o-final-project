const d3 = require('d3');
const fs = require('fs');
const _ = require('lodash');

_.range(1, 13).forEach((month) => {
  const data = fs.readFileSync(`raw_data/airbnb_raw_data/listings-2019-${month}.csv`, 'utf8');
  const parsedData = d3
    .csvParse(data)
    .filter(record => parseInt(record.availability_365) > 0)
    .map((record) => ({
      listingId: parseInt(record.id),
      hostId: parseInt(record.host_id),
      hostName: record.host_name,
      latitude: parseFloat(record.latitude),
      longitude: parseFloat(record.longitude),
      roomType: record.room_type,
      accommodates: parseInt(record.accommodates),
      bathrooms: parseFloat(record.bathrooms),
      bedrooms: parseInt(record.bedrooms),
      beds: parseInt(record.beds),
      price: parseFloat(record.price.replace('$', '')),
      rating: parseInt(record.review_scores_rating),
      reviews: parseInt(record.reviews_per_month),
      location: `${record.neighbourhood_cleansed}, ${record.neighbourhood_group_cleansed}, NY`,
    }))
    .filter((record) => {
      return !Number.isNaN(record.price) && record.roomType !== null && !Number.isNaN(record.rating) && !Number.isNaN(record.reviews) && record.price > 0;
    });
  fs.writeFileSync(`raw_data/airbnb-nyc-2019-${month}.json`, JSON.stringify(parsedData, null, 4));
});
