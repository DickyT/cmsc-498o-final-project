import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
import * as d3 from 'd3';

import Menu from 'antd/es/menu';
import Dropdown from 'antd/es/dropdown';
import Button from 'antd/es/button';
import {
  faChevronDown,
  faLocationArrow,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { AppContext } from '../App';
import MainMap from './MainMap';
import MapTooltip from './MapTooltip';
import SmartSearchBar from './SmartSearchBar';
import SideBar from './SideBar';

export enum MapType {
  AveragePrice = 'Averate Price',
  ListingCount = 'Listing Count',
  AverageRating = 'Averate Rating',
}

const MapBrowserContent = styled.div`
  width: 100%;
  height: 100%;
`;

const MapControlArea = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  display: flex;
  flex-direction: column;
  align-items: flex-end;

  & > button {
    border-radius: 100px;
    margin-top: 10px;
    margin-right: 15px;
  }
`;

const MapTitle = styled.div`
  padding: 10px;
  background: rgba(255, 255, 255, 0.75);
  border-radius: 10px;

  & > span {
    margin-left: 10px;
    margin-right: 10px;
    font-size: 20px;
  }
`;

interface IMapBrowserProps {

}

export interface TooltipInfo {
  location: string;
  pageX: number;
  pageY: number;
}

export type MapRenderData = Map<MapType, Map<string, number>>;

const DownArrowContent = styled.i`
  margin-left: 10px;
`;

function DownArrow() {
  return (
    <DownArrowContent>
      <FontAwesomeIcon icon={faChevronDown} />
    </DownArrowContent>
  );
}

const MapBrowser: React.FC<IMapBrowserProps> = () => {
  const { data } = React.useContext(AppContext);

  const [mapType, setMapType] = React.useState<MapType>(MapType.AveragePrice);
  const [selectedTime, setSelectedTime] = React.useState<string>('');
  const [tooltipInfo, setTooltipInfo] = React.useState<TooltipInfo | null>(null);
  const [sideBarOpen, setSideBarOpen] = React.useState<boolean>(false);
  const [selectedLocation, setSelectedLocation] = React.useState<string | null>(null);
  const [renderData, setRenderData] = React.useState<MapRenderData>(new Map());
  const [mapMoved, setMapMoved] = React.useState<boolean>(false);

  const selfRef = React.useRef<HTMLDivElement | null>(null);

  function onSelectLocation(location: string) {
    setSelectedLocation(location);
    if (!sideBarOpen) {
      setSideBarOpen(true);
    }
  }

  function onMouseMove(location: string, event: MouseEvent) {
    setTooltipInfo({
      location,
      pageX: event.pageX,
      pageY: event.pageY,
    });
  }

  function onMouseOut() {
    setTooltipInfo(null);
  }

  function onSideBarOpen() {
    setSideBarOpen(true);
  }

  function onSideBarClose() {
    setSideBarOpen(false);
  }

  function resetMap() {
    if (selfRef.current) {
      const mainMap = selfRef.current.querySelector('svg > g[transform]');
      if (mainMap) {
        const zoomData = (mainMap as any).zoom;
        if (zoomData) {
          const [zoomEle, svgZoom] = zoomData;
          zoomEle
            .transition()
            .duration(250)
            .call(svgZoom.transform, d3.zoomIdentity)
            .on('end', () => setMapMoved(false));
        }
      }
    }
  }

  function onTimeChange(time?: string) {
    if (!time) {
      time = data.getCurrentTime();
    }
    setSelectedTime(time);
    // change data model time
    data.setCurrentTime(time);
    // generate render data
    const renderData = new Map<MapType, Map<string, number>>();
    Object.keys(MapType).forEach((enumKey) => {
      const eachMapType = (MapType as any)[enumKey] as MapType;

      if (eachMapType === MapType.AveragePrice) {
        renderData.set(eachMapType, data.groupByMean<string>('location', 'price'));
      } else if (eachMapType === MapType.ListingCount) {
        renderData.set(eachMapType, data.groupByCount<string>('location'));
      } else if (eachMapType === MapType.AverageRating) {
        renderData.set(eachMapType, data.groupByMean<string>('location', 'rating'));
      }
    });
    // update to state
    setRenderData(renderData);
  }

  React.useEffect(onTimeChange, []);

  const mapTypeMenu = (
    <Menu onClick={({ key }) => setMapType(key as any)}>
      <Menu.Item key={MapType.AveragePrice}>
        {MapType.AveragePrice}
      </Menu.Item>
      <Menu.Item key={MapType.ListingCount}>
        {MapType.ListingCount}
      </Menu.Item>
      <Menu.Item key={MapType.AverageRating}>
        {MapType.AverageRating}
      </Menu.Item>
    </Menu>
  );

  const timeMenuList: JSX.Element[] = [];
  data.timeList.forEach((timeStr) => {
    const monthName = moment(timeStr, 'YYYY-MM').format('MMMM');
    timeMenuList.push(
      <Menu.Item key={timeStr}>
        {monthName}
      </Menu.Item>
    );
  });
  const timeMenu = (
    <Menu onClick={({ key }) => onTimeChange(key as any)}>
      {timeMenuList}
    </Menu>
  );

  return (
    <MapBrowserContent style={{ cursor: tooltipInfo ? 'pointer' : 'unset' }} ref={selfRef}>
      <MainMap
        selectedMapType={mapType}
        onClick={onSelectLocation}
        onMouseMove={onMouseMove}
        onMouseOut={onMouseOut}
        onMapMove={() => setMapMoved(true)}
        renderData={renderData}
      />
      <SmartSearchBar
        onSideBarOpen={onSideBarOpen}
        onSelect={onSelectLocation}
        renderData={renderData}
      />
      <MapControlArea>
        <MapTitle>
          <Dropdown overlay={mapTypeMenu}>
            <Button>
              {mapType} <DownArrow />
            </Button>
          </Dropdown>
          <span>of NYC in</span>
          <Dropdown overlay={timeMenu}>
            <Button>
              {moment(selectedTime, 'YYYY-MM').format('MMMM')} <DownArrow />
            </Button>
          </Dropdown>
          <span>2019</span>
        </MapTitle>
        <Button
          style={{ display: mapMoved ? 'unset' : 'none' }}
          onClick={resetMap}
        >
          <FontAwesomeIcon icon={faLocationArrow} />
        </Button>
      </MapControlArea>
      <MapTooltip
        tooltipInfo={tooltipInfo}
        renderData={renderData}
      />
      <SideBar
        open={sideBarOpen}
        onSideBarClose={onSideBarClose}
        selectedLocation={selectedLocation}
        selectedTime={selectedTime}
        renderData={renderData}
      />
    </MapBrowserContent>
  );
};

export default MapBrowser;
