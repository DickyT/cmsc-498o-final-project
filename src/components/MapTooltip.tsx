import React from 'react';
import styled from 'styled-components';
import Card from 'antd/es/card';
import _ from 'lodash';

import { TooltipInfo, MapType, MapRenderData } from './MapBrowser';

const TooltipContainer = styled.div`
  position: absolute;
  top: 15px;
  left: 15px;
  pointer-events: none;
  width: 350px;
  height: 200px;
  
  & > div {
    border: 1px solid #E0E0E0;
    border-radius: 5px;
  }
`;

function MapTooltip({ tooltipInfo, renderData }: { tooltipInfo: TooltipInfo | null, renderData: MapRenderData }) {
  let content: JSX.Element | null = null;
  const tooltipStyle: React.CSSProperties = { display: tooltipInfo ? 'unset' : 'none' };

  if (tooltipInfo !== null && renderData.size >= 3) {
    const { location } = tooltipInfo;
    let { pageX, pageY } = tooltipInfo;

    const avgPrice = renderData.get(MapType.AveragePrice)!.get(location);
    const count = renderData.get(MapType.ListingCount)!.get(location);
    const avgRating = renderData.get(MapType.AverageRating)!.get(location);

    if (!avgPrice || !count || !avgRating) {
      content = (
        <p>
          There is no listings in this area.
        </p>
      );
    } else {
      content = (
        <>
          <p>
            There are {count} listings in this area.
          </p>
          <p>
            Average listing price is ${_.round(avgPrice, 2)}.
          </p>
          <p>
            The average Airbnb rating in this area is {_.round(avgRating, 2)}/100
          </p>
        </>
      );
    }

    tooltipStyle.transform = `translate(${pageX}px, ${pageY}px)`;

    if (pageX > (window.innerWidth - 400)) {
      tooltipStyle.right = 'calc(100% + 15px)';
      tooltipStyle.left = 'unset';
    }

    if (pageY > (window.innerHeight - 250)) {
      tooltipStyle.bottom = 'calc(100% + 15px)';
      tooltipStyle.top = 'unset';
    }
  }

  return (
    <TooltipContainer style={tooltipStyle}>
      <Card title={tooltipInfo?.location} >
        {content}
      </Card>
    </TooltipContainer >
  );
}

export default MapTooltip;
