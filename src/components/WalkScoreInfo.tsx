import React from 'react';
import styled from 'styled-components';
import { loadWalkScoreData, WalkScoreData } from '../utils/utils';

const Content = styled.div`
  margin: 15px 15px 10px 15px;
  border: 1px solid #ececec;
  padding: 10px 0;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const WalkScoreLogo = styled.a`
  display: flex;
  justify-content: center;

  & img {
    width: 50%;
  }
`;

const SmallInfo = styled.div`
  margin: 10px 20px;
`;

const MainArea = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  padding: 0 20px;

  & p {
    margin: 0;
  }
`;

const ScoreArea = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const TransitArea = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  flex-direction: column;

  & p {
    margin: 0;
  }
`;

const Info = styled.p`
  margin-top: 20px;
`;

const WalkScoreInfo: React.FC<{ location: string }> = ({ location }) => {
  const [data, setData] = React.useState<WalkScoreData | null | false>(null);

  async function loadData(location: string) {
    setData(null);
    const walkData = await loadWalkScoreData(location);
    if (walkData) {
      setData(walkData);
      return true;
    } else {
      setData(false);
    }
    return false;
  }

  async function didMount(location: string) {
    const detailedLocation = location;
    if (await loadData(detailedLocation)) {
      // nice we success
    } else {
      // try bigger area
      const generalLocation = `${detailedLocation.split(',')[1].trim()}, NY`;
      console.log(generalLocation);
      loadData(generalLocation);
    }
  }

  React.useEffect(() => {
    didMount(location);
  }, [location]);

  let content: JSX.Element;

  if (data === null) {
    content = (
      <Info>Loading...</Info>
    );
  } else if (data === false) {
    content = (
      <Info>Data not available for this area.</Info>
    );
  } else {
    let transitContent: JSX.Element | null = null;
    if (data.dtTraffic) {
      transitContent = (
        <TransitArea>
          <p>Distance to Downtown</p>
          <p>Drive: <strong>{data.dtTraffic.drive}</strong> mins</p>
          <p>Public Trans.: <strong>{data.dtTraffic.transit}</strong> mins</p>
          <p>Bike: <strong>{data.dtTraffic.bike}</strong> mins</p>
          <p>Walk: <strong>{data.dtTraffic.walk}</strong> mins</p>
        </TransitArea>
      );
    }

    content = (
      <>
        <SmallInfo>
          This area is the {data.scores.rank}th most walkable area, with {data.scores.residentsCount} residents.
        </SmallInfo>
        <MainArea>
          <ScoreArea>
            <p>Walk Score: <strong>{data.scores.walk}</strong></p>
            <p>Transit Score: <strong>{data.scores.transit}</strong></p>
            <p>Bike Score: <strong>{data.scores.bike}</strong></p>
          </ScoreArea>
          {transitContent}
        </MainArea>
      </>
    );
  }

  return (
    <Content>
      <WalkScoreLogo href="https://www.walkscore.com/" target="_blank">
        <img src="https://www.davidalber.net/images/walkscore-logo.png" alt="" />
      </WalkScoreLogo>
      {content}
    </Content>
  );
};

export default WalkScoreInfo;
