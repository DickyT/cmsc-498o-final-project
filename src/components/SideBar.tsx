import React from 'react';
import moment from 'moment';
import styled from 'styled-components';
import {
  faCaretLeft,
  faShareSquare
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import PriceTypePieChart from './PriceTypePieChart';
import RatingBarChart from './RatingBarChart';
import WalkScoreInfo from './WalkScoreInfo';
import ReviewsLineChart from './ReviewsLineChart';
import { MapRenderData, MapType } from './MapBrowser';

const SideBarContainer = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: -430px;
  z-index: 1;
  width: 430px;
  background-color: white;
  box-shadow: none;
  transition: all 0.2s;
  padding-top: 80px;
  overflow: scroll;

  ::-webkit-scrollbar {
    display: none;
  }

  &.open {
    left: 0;
    box-shadow: 0 -1px 24px rgba(0,0,0,0.4);
  }

  & > p {
    text-align: center;
    margin-top: 25px;
  }
`;

const LocationSummary = styled.div`
  
`;

const Title = styled.div`
  font-size: 18px;
  text-align: center;
  margin: 0 15px;
  border: 1px solid #ececec;
  padding: 10px 0;

  & > svg {
    float: right;
    transform: translate(-10px, 4px);
    cursor: pointer;
  }
`;

const SideBarContent = styled.div`
  margin: 0 15px;
  overflow: hidden;
`;

const ScrollBarOffset = styled.div`
  height: calc(100vh - 100px);
  overflow-y: scroll;
  margin-right: -25px;
  text-align: center;
`;

const TimeTitle = styled.h3`
  text-align: center;
  margin: 10px 0;
`;

const HideButton = styled.div`
  position: absolute;
  right: 0;
  top: 15px;
  width: 0;
  height: 45px;
  background-color: rgba(255,255,255,0.9);
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: none;
  cursor: pointer;
  transition: all 0.2s;
  overflow: hidden;

  & svg {
    margin-left: -2px;
  }

  &:hover {
    background-color: #E0E0E0E6;
  }

  &:active {
    background-color: #BDBDBDE6;
  }

  ${SideBarContainer}.open & {
    width: 25px;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.3);
  }
`;

interface ISideBarProps {
  open: boolean;
  selectedLocation: string | null;
  selectedTime: string;
  onSideBarClose?: () => void
  renderData: MapRenderData;
}

const SideBar: React.FC<ISideBarProps> = ({ open, selectedLocation, selectedTime, onSideBarClose, renderData }) => {
  let content: JSX.Element | null = null;

  if (selectedLocation) {
    const avgPrice = renderData.get(MapType.AveragePrice)!.get(selectedLocation);
    const count = renderData.get(MapType.ListingCount)!.get(selectedLocation);
    const avgRating = renderData.get(MapType.AverageRating)!.get(selectedLocation);

    if (!avgPrice || !count || !avgRating) {
      content = (
        <p>
          There is no listings in this area.
        </p>
      );
    } else {
      content = (
        <SideBarContent>
          <PriceTypePieChart time={selectedTime} location={selectedLocation} />
          <RatingBarChart time={selectedTime} location={selectedLocation} />
          <ReviewsLineChart time={selectedTime} location={selectedLocation} />
        </SideBarContent>
      );
    }
  }

  let openMapsBtn: JSX.Element | null = null;
  if (selectedLocation) {
    openMapsBtn = (
      <FontAwesomeIcon icon={faShareSquare} onClick={() => window.open(`https://www.google.com/maps?q=${encodeURIComponent(selectedLocation)}`)} />
    );
  }

  return (
    <SideBarContainer className={open ? 'open' : ''}>
      <LocationSummary>
        <Title>
          {selectedLocation || 'Select a Region to Analysis'}
          {openMapsBtn}
        </Title>
        <TimeTitle>{moment(selectedTime, 'YYYY-MM').format('MMMM, YYYY')}</TimeTitle>
        {selectedLocation ? <WalkScoreInfo location={selectedLocation} /> : null}
      </LocationSummary>
      {content}
      <HideButton onClick={onSideBarClose}>
        <FontAwesomeIcon icon={faCaretLeft} />
      </HideButton>
    </SideBarContainer>
  );
};

export default SideBar;
