import * as d3 from 'd3';
import React from 'react';
import styled from 'styled-components';
import { legendColor } from 'd3-svg-legend';

import { MapType } from './MapBrowser';
import { AppContext } from '../App';
import { GeoJsonData, loadNYCGeoJson, GeoJsonFeature } from '../utils/utils';

const MainSVG = styled.svg`
  
`;

const SVGContent = styled.g`
  
`;

const MapContent = styled.g`
  & > path {
    stroke-width: 0.2px;
    stroke: white;
    transition: 0.2s stroke, 0.2s stroke-width;
  }

  & > path:hover {
    stroke-width: 2px;
    stroke: #E91E63;
  }
`;

const LegendContainer = styled.foreignObject`
  width: 100vw;
  height: 100vh;
  pointer-events: none;
`;

const LegendBackground = styled.div`
  position: absolute;
  right: 15px;
  bottom: 15px;
  padding: 15px;
  background: rgba(255, 255, 255, 0.75);
  border-radius: 10px;
`;

const LegendContent = styled.svg`
  width: 120px;
  height: 165px;

  & .legendCells > .cell:last-child {
    display: none;
  }
`;

export type MapRenderData = Map<MapType, Map<string, number>>;

interface IMapProps {
  selectedMapType: MapType;
  onClick?: (location: string, event: MouseEvent) => void;
  onMouseMove?: (location: string, event: MouseEvent) => void;
  onMouseOut?: (location: string, event: MouseEvent) => void;
  onMapMove?: () => void;
  renderData: MapRenderData;
}

interface IMapState {
  mapGeoJson: GeoJsonData | null;
  width: number;
  height: number;
}

const NO_DATA_COLOR = '#616161';
const MAP_SCALE = 78000 / 768;

class MainMap extends React.Component<IMapProps, IMapState> {
  private width = window.innerWidth;
  private height = window.innerHeight;

  private svg = React.createRef<SVGSVGElement>();
  private svgContent = React.createRef<SVGGElement>();
  private mapContent = React.createRef<SVGGElement>();
  private legendContent = React.createRef<SVGSVGElement>();

  state: IMapState = {
    mapGeoJson: null,
    width: window.innerWidth,
    height: window.innerHeight,
  };

  setupSvg() {
    // setup svg
    const svg = d3.select(this.svg.current!);
    // setup zoom
    const svgZoom = d3
      .zoom()
      .scaleExtent([0.8, 8])
      .on('zoom', () => {
        this.props.onMapMove && this.props.onMapMove();
        this.svgContent.current!.setAttribute('transform', d3.event.transform);
      }) as any;
    const zoomEle = svg.call(svgZoom);
    (this.svgContent.current! as any).zoom = [zoomEle, svgZoom];
    svg.on('dblclick.zoom', null);
  }

  async loadMap() {
    // load nyc geojson
    this.setState({ mapGeoJson: (await loadNYCGeoJson()) });
  }

  componentDidUpdate(prevProps: IMapProps, prevState: IMapState) {
    if (prevState.mapGeoJson !== this.state.mapGeoJson && this.state.mapGeoJson !== null) {
      // need to (re)load map
      const mapGeoMercator = d3
        .geoMercator()
        .scale(MAP_SCALE * this.height);
      // setup projection
      const geoJsonProjection = d3
        .geoPath()
        .projection(mapGeoMercator);
      // remove old content
      d3
        .select(this.mapContent.current!)
        .selectAll('path')
        .remove();
      // draw map
      d3
        .select(this.mapContent.current!)
        .selectAll('path')
        .data(this.state.mapGeoJson.features)
        .enter()
        .append('path')
        .attr('d', geoJsonProjection as any)
        .on('click', ({ properties: { location } }) => {
          this.props.onClick && this.props.onClick(location, d3.event);
        })
        .on('mousemove', ({ properties: { location } }) => {
          this.props.onMouseMove && this.props.onMouseMove(location, d3.event);
        })
        .on('mouseout ', ({ properties: { location } }) => {
          this.props.onMouseOut && this.props.onMouseOut(location, d3.event);
        });
      // center the map
      const { x, y, width, height } = this.mapContent.current!.getBBox();
      const mapX = x + width / 2;
      const mapY = y + height / 2;
      const centerX = this.width / 2;
      const centerY = this.height / 2;
      this.mapContent.current!.setAttribute('transform', `translate(${centerX - mapX}, ${centerY - mapY})`);
      // load encoder
      this.encodeDataToMap();
    }
    const mapTypeChanged = prevProps.selectedMapType !== this.props.selectedMapType;
    const renderDataChanged = prevProps.renderData !== this.props.renderData;
    if (mapTypeChanged || renderDataChanged) {
      // reload encoder
      this.encodeDataToMap();
    }
  }

  encodeDataToMap() {
    // generate render data
    const { selectedMapType, renderData } = this.props;

    const color = d3
      .scaleQuantize<string>()
      .range(['#80CBC4', '#4DB6AC', '#26A69A', '#009688', '#00897B', '#00796B', '#00695C', '#004D40', '#00796B']);

    const currentRenderData: Map<string, number> = renderData.get(selectedMapType)!;

    // Scale color
    color.domain(d3.extent([...currentRenderData.values()]) as [number, number]);

    // generate legend
    const legend = legendColor()
      .labelFormat(d3.format('d'))
      .scale(color);

    // use legend
    d3
      .select(this.legendContent.current!)
      .call(legend as any);

    // Render
    d3
      .select(this.mapContent.current!)
      .selectAll('path')
      .attr('fill', (rawData: any) => {
        const { properties: { location } } = rawData as GeoJsonFeature;
        if (currentRenderData.has(location)) {
          return color(currentRenderData.get(location)!);
        }
        return NO_DATA_COLOR;
      });
  }

  onResize = () => {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  async componentDidMount() {
    // Assign context type
    MainMap.contextType = AppContext;
    // initial setup
    this.setupSvg();
    await this.loadMap();
    // follow screen size
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  render() {
    return (
      <MainSVG width={this.state.width} height={this.state.height} ref={this.svg}>
        <SVGContent ref={this.svgContent}>
          <MapContent ref={this.mapContent} />
        </SVGContent>
        <LegendContainer>
          <LegendBackground>
            <LegendContent ref={this.legendContent} />
          </LegendBackground>
        </LegendContainer>
      </MainSVG>
    );
  }
}

export default MainMap;
