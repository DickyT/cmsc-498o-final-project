import React from 'react';
import styled from 'styled-components';
import Fuse from 'fuse.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faBars,
  faSearch,
  faMapMarkerAlt,
} from '@fortawesome/free-solid-svg-icons';

import { MapType, MapRenderData } from './MapBrowser';
import IconButton from '../utils/IconButton';

const SearchBarContent = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  margin: 15px;
`;

const SearchBar = styled.div`
  position: relative;
  width: 375px;
  height: 45px;
  border-radius: 6px;
  box-shadow: rgba(0, 0, 0, 0.2) 0px 2px 4px 0px, rgba(0, 0, 0, 0.02) 0px -1px 0px 0px;
  overflow: hidden;
  display: inline-flex;
  justify-content: space-between;
  align-items: center;
  background-color: white;
  z-index: 3;
  padding: 0 5px;
`;

const Input = styled.input`
  flex: 1;
  height: 100%;
  font-size: 15px;
  border: none;
  outline: none;
  padding: 5px;
  color: #424242;

  &::placeholder {
    color: #9E9E9E;
  }
`;

const Suggestions = styled.div`
  position: absolute;
  top: 39px;
  left: 0;
  right: 0;
  z-index: 2;
  padding: 0;
  background-color: white;
  box-shadow: rgba(0, 0, 0, 0.2) 0px 2px 4px 0px, rgba(0, 0, 0, 0.02) 0px -1px 0px 0px;
  border-bottom-left-radius: 6px;
  border-bottom-right-radius: 6px;
  overflow: hidden;
  height: 0;
  transition: all 0.2s;

  &.focused {
    height: auto;
    padding: 20px 0 10px 0;
    max-height: calc(100vh - 100px);
  }
`;

const ScrollBarOffset = styled.div`
  margin-right: -20px;
  max-height: calc(360px);
  overflow-y: scroll;
`;

const ResultContent = styled.div`
  padding: 10px 20px;
  transition: background-color 0.2s;
  cursor: pointer;
  font-size: 14px;
  display: flex;
  align-items: center;
  justify-content: flex-start;

  &:hover {
    background-color: #EEEEEE;
  }
`;

const RightButtons = styled.div`
  & > * {
    border-right: 1px solid #EEEEEE;
  }

  & > *:last-child {
    border-right: none;
  }
`;



const Result: React.FC<{ location: string, onSelect?: () => void }> = ({ location, onSelect }) => {
  return (
    <ResultContent onClick={onSelect}>
      <FontAwesomeIcon icon={faMapMarkerAlt} style={{ marginRight: '15px' }} />
      <span>{location}</span>
    </ResultContent>
  );
};

interface ISmartSearchBarProps {
  onSideBarOpen?: () => void;
  onSelect?: (location: string) => void;
  renderData: MapRenderData;
}

const SmartSearchBar: React.FC<ISmartSearchBarProps> = ({ onSideBarOpen, onSelect, renderData }) => {
  const containerRef = React.useRef<HTMLDivElement>(null);
  const inputRef = React.useRef<HTMLInputElement>(null);
  const fuseSearch = React.useRef<Fuse<string, {}> | null>(null);

  const [focused, setFocused] = React.useState<boolean>(false);
  const [searchResult, setSearchResult] = React.useState<Fuse.FuseResult<string>[]>([]);

  function onFocus() {
    setFocused(true);
  }

  function onBlur() {
    setFocused(false);
  }

  React.useEffect(() => {
    // global click event
    function globalOnMouseDown({ target }: MouseEvent) {
      // console.log(renderData);
      if (containerRef.current && !containerRef.current.contains(target as any)) {
        onBlur();
      }
    }
    // handle click outside
    window.addEventListener('click', globalOnMouseDown);
    return () => {
      window.removeEventListener('click', globalOnMouseDown);
    };
  }, [renderData]);

  function onInput({ target: { value } }: React.ChangeEvent<HTMLInputElement>) {
    if (!fuseSearch.current) {
      const categoryData = renderData.get(MapType.AveragePrice);
      if (categoryData) {
        fuseSearch.current = new Fuse([...categoryData.keys()], {});
      }
    }

    if (fuseSearch.current) {
      setSearchResult(fuseSearch.current.search(value));
    }
  }

  const suggestionContent: JSX.Element[] = [];

  searchResult.forEach(({ item, refIndex }) => {
    const callback = () => {
      if (onSelect) {
        onSelect(item);
      }
      // hide suggestion
      setFocused(false);
    };
    suggestionContent.push(<Result key={refIndex} location={item} onSelect={callback} />);
  });

  return (
    <SearchBarContent onFocus={onFocus} ref={containerRef}>
      <SearchBar>
        <IconButton icon={faBars} onClick={onSideBarOpen} />
        <Input
          placeholder="Search for a Location or Region"
          onChange={onInput}
          ref={inputRef}
        />
        <RightButtons>
          <IconButton icon={faSearch} onClick={() => inputRef.current?.focus()} />
          {/* <IconButton icon={faTimes} onClick={() => inputRef.current?.focus()} /> */}
        </RightButtons>
      </SearchBar>
      <Suggestions className={focused && searchResult.length > 0 ? 'focused' : ''}>
        <ScrollBarOffset>
          {suggestionContent}
        </ScrollBarOffset>
      </Suggestions>
    </SearchBarContent>
  );
};

export default SmartSearchBar;
