import React from 'react';
import * as d3 from 'd3';
import _ from 'lodash';
import styled from 'styled-components';
import {
  faDollarSign,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { AppContext } from '../App';

interface IVizProps {
  location: string;
  time: string;
}

const Content = styled.div`
  border-top: 1px solid #ececec;
  margin-right: 10px;
  padding-top: 15px;
  margin-bottom: 15px;
`;

const Banner = styled.div`
  text-align: center;
  font-size: 1.3em;
  background: #EEEEEE;
  padding: 5px 10px;
  border-radius: 5px;

  & > svg {
    margin-right: 2px;
  }
`;

const MainSVG = styled.svg`
  width: 100%;


  & .pie-path {
    stroke: #9E9E9E;
    stroke-width: 1px;
  }

  & .pie-text {
    text-anchor: middle;
    fill: white;
  }
`;

const Report = styled.div`
  & > div > span {
    font-weight: bold;
  }
`;

type VizEntry = { name: string, value: number };

const PriceTypePieChart: React.FC<IVizProps> = ({ location, time }) => {
  const { data } = React.useContext(AppContext);

  const svgRef = React.useRef<SVGSVGElement | null>(null);
  const svgContent = React.useRef<SVGGElement | null>(null);
  const reportRef = React.useRef<HTMLDivElement | null>(null);

  const [avg, setAvg] = React.useState<number>(0);

  React.useEffect(() => {
    if (time === data.getCurrentTime()) {
      const svg = svgRef.current;
      const g = svgContent.current;
      if (svg && g) {
        // generate data
        const renderData = data.groupByCount<string>('roomType', d => d.location === location);
        const dataList: VizEntry[] = [];
        for (const [name, value] of renderData.entries()) {
          dataList.push({ name, value });
        }
        // console.log(dataList);
        // offset to center
        const size = svg.getBoundingClientRect();
        const padding = 20;
        const width = size.width;
        const height = size.height - padding * 2;
        const radius = Math.min(width, height) / 2;
        // console.log(radius);
        g.setAttribute('transform', `translate(${width / 2}, ${height / 2 + padding})`);
        // set up color
        const typeList = dataList.map(d => d.name);
        // console.log(typeList);
        const color = d3.scaleOrdinal(d3.schemeTableau10).domain(typeList);
        // draw pie chart
        const pie = d3
          .pie()
          .value((d: any) => d.value);
        const pieData = pie(dataList as any);
        const arc = d3
          .arc()
          .innerRadius(0)
          .outerRadius(radius);
        const newData = d3
          .select(g)
          .selectAll('path')
          .data(pieData);
        newData.exit().remove();
        newData.enter()
          .append('path');
        d3
          .select(g)
          .selectAll('path')
          .data(pieData)
          .attr('class', 'pie-path')
          .attr('fill', (d: any) => {
            return color(d.data.name);
          })
          .attr('d', arc as any);
        d3
          .select(g)
          .selectAll('text')
          .remove();
        d3
          .select(g)
          .selectAll('text')
          .data(pieData)
          .enter()
          .append('text')
          .attr('transform', (d: any) => `translate(${arc.centroid(d)})`)
          .attr('class', 'pie-text')
          .attr('dy', '.35em')
          .text((d: any) => {
            const angleOffset = d.endAngle - d.startAngle;
            const rate = angleOffset / (Math.PI * 2);
            if (rate < 0.15) {
              return '';
            }
            return `${_.round(rate * 100, 2)}%`;
          });
        // generate strings
        const report = reportRef.current;
        if (report) {
          const rawData = data.groupBy<string>('roomType', d => d.location === location);
          report.innerHTML = '';
          const elements = typeList.map((eachType) => {
            const typeData = rawData.get(eachType)!;
            const avgPrice = _.round(_.meanBy(typeData, 'price'), 2);
            const thisColor = color(eachType);
            return `<div style="text-align: center;">Average Price for <span style="color:${thisColor}">${eachType}</span> is <span style="color:${thisColor}">$${avgPrice}</span></div>`;
          });
          report.innerHTML = elements.join('\n');
        }
      }
      // update overall avg
      const avgData = data.groupByMean<string>('location', 'price', d => d.location === location);
      const mean = _.round(avgData.get(location)!, 2);
      setAvg(mean);
    }
  }, [location, time, data]);

  return (
    <Content>
      <Banner>Overall Average Price is <FontAwesomeIcon icon={faDollarSign} />{avg}</Banner>
      <MainSVG ref={svgRef}>
        <g ref={svgContent} />
      </MainSVG>
      <Report ref={reportRef} />
    </Content>
  );
};

export default PriceTypePieChart;
