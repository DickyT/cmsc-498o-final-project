import React from 'react';
import * as d3 from 'd3';
import _ from 'lodash';
import moment from 'moment';
import styled from 'styled-components';

import { AppContext } from '../App';

interface IVizProps {
  location: string;
  time: string;
}

const Content = styled.div`
  border-top: 1px solid #ececec;
  margin-right: 10px;
  padding-top: 15px;
  margin-bottom: 15px;
`;

const Banner = styled.div`
  text-align: center;
  font-size: 1.3em;
  background: #EEEEEE;
  padding: 5px 10px;
  border-radius: 5px;

  & > svg {
    margin-right: 2px;
  }
`;

const MainSVG = styled.svg`
  width: 100%;
  height: 200px;
  margin-top: 20px;

  & .helper-line {
    pointer-events: none;
    stroke: grey;
    stroke-width: 1px;
  }

  & .line-path {
    fill: none;
    stroke-width: 2px;
  }
`;

const Report = styled.div`
  & > div > span {
    font-weight: bold;
  }
`;

type VizEntry = { name: string, value: number };

const ReviewsLineChart: React.FC<IVizProps> = ({ location, time }) => {
  const { data } = React.useContext(AppContext);

  const svgRef = React.useRef<SVGSVGElement | null>(null);
  const svgContent = React.useRef<SVGGElement | null>(null);
  const reportRef = React.useRef<HTMLDivElement | null>(null);
  const xAxisRef = React.useRef<SVGGElement | null>(null);
  const yAxisRef = React.useRef<SVGGElement | null>(null);
  const textRef = React.useRef<SVGGElement | null>(null);

  React.useEffect(() => {
    if (time === data.getCurrentTime()) {
      const svg = svgRef.current;
      const g = svgContent.current;
      const xAxis = xAxisRef.current;
      const yAxis = yAxisRef.current;
      const text = textRef.current;
      if (svg && g && xAxis && yAxis && text) {
        // generate data
        const renderData = data.reviewsDataByRoomType(location);
        // convert data
        const typeList = ['Entire home/apt', 'Private room', 'Shared room'];
        const dataList: { type: string, data: { time: moment.Moment, data: number }[] }[] = [];
        const yDataRaw: number[] = [];
        typeList.forEach((typeName) => {
          const timedData: { time: moment.Moment, data: number }[] = [];
          renderData.forEach(({ time, data }) => {
            const currentData = data.find(d => d.type === typeName);
            if (currentData) {
              timedData.push({
                time,
                data: currentData.value,
              });
              yDataRaw.push(currentData.value);
            }
          });
          dataList.push({
            type: typeName,
            data: timedData,
          });
        });
        // offset to center
        const size = svg.getBoundingClientRect();
        const padding = 25;
        const width = size.width - padding * 2;
        const height = size.height - padding * 2;
        g.setAttribute('transform', `translate(${padding}, ${padding})`);
        // set up color
        const color = d3.scaleOrdinal(d3.schemeTableau10).domain(typeList);
        // draw
        const xScale = d3
          .scaleTime()
          .domain(d3.extent(dataList[0].data, d => d.time.toDate()) as any)
          .range([0, width]);
        const yScale = d3
          .scaleLinear()
          .domain([0, _.max(yDataRaw)!])
          .range([height, 0]);

        const line = d3.line()
          .x((d: any) => xScale(d.time.toDate()))
          .y((d: any) => yScale(d.data));

        d3.select(g).html('');

        typeList.forEach((typeName) => {
          const foundRawData = dataList.find(d => d.type === typeName);
          if (foundRawData) {
            d3
              .select(g)
              .append('path')
              .data([foundRawData.data])
              .attr('class', 'line-path')
              .attr('stroke', color(typeName))
              .attr('d', line as any);
          }
        });
        // render axis
        d3
          .select(xAxis)
          .html('')
          .attr('class', 'bottom-axis')
          .attr('transform', `translate(${padding}, ${height + padding})`)
          .call(d3.axisBottom(xScale).tickFormat(d3.timeFormat('%b') as any));
        d3
          .select(yAxis)
          .html('')
          .attr('transform', `translate(${padding}, ${padding})`)
          .call(d3.axisLeft(yScale));
        // assign events
        const helperLine = d3
          .select(g)
          .append('line')
          .attr('class', 'helper-line')
          .attr('x1', 0)
          .attr('y1', 0)
          .attr('x2', 0)
          .attr('y2', height);
        d3
          .select(svg)
          .on('mousemove', function () {
            let selectedTime = xScale.invert(d3.mouse(this)[0] - padding);
            if (selectedTime.getDate() >= 15) {
              if (selectedTime.getMonth() < 12) {
                selectedTime.setMonth(selectedTime.getMonth() + 1);
              }
            }
            selectedTime.setHours(0, 0, 0, 0);
            selectedTime.setDate(1);

            const lineX = xScale(selectedTime);
            helperLine
              .attr('x1', lineX)
              .attr('x2', lineX);

            if (reportRef.current) {
              const textList: string[] = [];
              typeList.forEach((typeName) => {
                const foundRawData = dataList.find(d => d.type === typeName);
                const thisColor = color(typeName);
                let num = 0;
                if (foundRawData) {
                  const currentNumber = foundRawData.data.find(d => d.time.toDate().getMonth() === selectedTime.getMonth());
                  if (currentNumber) {
                    num = currentNumber.data;
                  }
                }
                textList.push(`<div style="text-align: center;">There are <strong>${num}</strong> reviews on ${moment(selectedTime).format('MMMM')} for <strong style="color:${thisColor}">${typeName}</strong></div>`);
              });
              reportRef.current.innerHTML = textList.join('\n');
            }
          })
      }
    }
  }, [location, time, data]);

  return (
    <Content>
      <Banner>Number of Reviews by Month</Banner>
      <MainSVG ref={svgRef}>
        <g ref={svgContent} />
        <g ref={xAxisRef} />
        <g ref={yAxisRef} />
        <g ref={textRef} />
      </MainSVG>
      <Report ref={reportRef} />
    </Content>
  );
};

export default ReviewsLineChart;
