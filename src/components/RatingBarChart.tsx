import React from 'react';
import * as d3 from 'd3';
import _ from 'lodash';
import styled from 'styled-components';

import { AppContext } from '../App';

interface IVizProps {
  location: string;
  time: string;
}

const Content = styled.div`
  border-top: 1px solid #ececec;
  margin-right: 10px;
  padding-top: 15px;
  margin-bottom: 15px;
`;

const Banner = styled.div`
  text-align: center;
  font-size: 1.3em;
  background: #EEEEEE;
  padding: 5px 10px;
  border-radius: 5px;

  & > svg {
    margin-right: 2px;
  }
`;

const MainSVG = styled.svg`
  width: 100%;
  height: 200px;
  margin-top: 20px;

  & .axis-label {
    text-anchor: end;
    font-size: 11px;
  }

  & .bar-label {
    text-anchor: middle;
  }
`;

const Report = styled.div`
  & > div > span {
    font-weight: bold;
  }
`;

type VizEntry = { name: string, value: number };

const RatingBarChart: React.FC<IVizProps> = ({ location, time }) => {
  const { data } = React.useContext(AppContext);

  const svgRef = React.useRef<SVGSVGElement | null>(null);
  const svgContent = React.useRef<SVGGElement | null>(null);
  const reportRef = React.useRef<HTMLDivElement | null>(null);
  const xAxisRef = React.useRef<SVGGElement | null>(null);
  const yAxisRef = React.useRef<SVGGElement | null>(null);
  const textRef = React.useRef<SVGGElement | null>(null);

  const [avg, setAvg] = React.useState<number>(0);

  React.useEffect(() => {
    if (time === data.getCurrentTime()) {
      const svg = svgRef.current;
      const g = svgContent.current;
      const xAxis = xAxisRef.current;
      const yAxis = yAxisRef.current;
      const text = textRef.current;
      if (svg && g && xAxis && yAxis && text) {
        // generate data
        const renderData = data
          .groupByMean<string>('roomType', 'rating', d => d.location === location);
        const dataList: VizEntry[] = [];
        for (const [name, value] of renderData.entries()) {
          dataList.push({ name, value });
        }
        console.log(dataList);
        // offset to center
        const size = svg.getBoundingClientRect();
        const padding = 20;
        const width = size.width - padding * 2;
        const height = size.height - padding * 2;
        g.setAttribute('transform', `translate(${padding}, 0)`);
        // set up color
        const typeList = dataList.map(d => d.name);
        // console.log(typeList);
        const color = d3.scaleOrdinal(d3.schemeTableau10).domain(typeList);
        // draw
        const xScale = d3.scaleBand()
          .range([0, width])
          .padding(0.5);
        const yScale = d3.scaleLinear()
          .range([height, 0]);

        xScale.domain(dataList.map(d => d.name));
        yScale.domain([0, d3.max(dataList, d => d.value)!]);

        const newData = d3.select(g)
          .selectAll('rect')
          .data(dataList);
        newData.exit().remove();
        newData
          .enter()
          .append('rect');
        d3.select(g)
          .selectAll('rect')
          .data(dataList)
          .attr('x', d => xScale(d.name)!)
          .attr('y', d => yScale(d.value)! + padding)
          .style('fill', d => color(d.name))
          .attr('width', xScale.bandwidth())
          .attr('height', d => height - yScale(d.value));
        // render axis
        d3
          .select(xAxis)
          .html('')
          .attr('class', 'bottom-axis')
          .attr('transform', `translate(${padding}, ${height + padding})`)
          .call(d3.axisBottom(xScale));
        d3
          .select(yAxis)
          .html('')
          .attr('transform', `translate(${padding}, ${padding})`)
          .call(d3.axisLeft(yScale));
        // render labels
        const textArea = d3.select(text)
        textArea.html('');
        // bar labels
        textArea
          .selectAll('text')
          .data(dataList)
          .enter()
          .append('text')
          .attr('class', 'bar-label')
          .attr('x', d => xScale(d.name)! + xScale.bandwidth() / 2 + padding)
          .attr('y', d => yScale(d.value)! + padding - 5)
          .text(d => _.round(d.value, 2));
        // axis label
        textArea
          .append('text')
          .attr('transform', `translate(${width + 30}, ${height + padding * 2 - 3})`)
          .attr('class', 'axis-label')
          .text('Type');
        textArea
          .append('text')
          .attr('transform', `translate(${padding + 35}, ${padding - 8})`)
          .attr('class', 'axis-label')
          .text('Avg Rating');
        // update rating
        const avgData = data
          .groupByMean<string>('location', 'rating', d => d.location === location);
        const mean = _.round(avgData.get(location)!, 2);
        setAvg(mean);
      }
    }
  }, [location, time, data]);

  return (
    <Content>
      <Banner>Overall Average Rating is {avg} / 100</Banner>
      <MainSVG ref={svgRef}>
        <g ref={svgContent} />
        <g ref={xAxisRef} />
        <g ref={yAxisRef} />
        <g ref={textRef} />
      </MainSVG>
      <Report ref={reportRef} />
    </Content>
  );
};

export default RatingBarChart;
