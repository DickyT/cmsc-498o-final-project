import fetch from 'node-fetch';
import AirbnbData from './AirbnbData';

export interface AirbnbListingStringProps {
  roomType: string;
  hostName: string;
  location: string;
}

export interface AirbnbListingNumberProps {
  listingId: number;
  hostId: number;
  latitude: number;
  longitude: number;
  accommodates: number;
  bathrooms: number;
  bedrooms: number;
  beds: number;
  price: number;
  rating: number;
  reviews: number;
}

export interface GeoJsonGeometry {
  type: 'MultiPolygon';
  coordinates: [number, number][][];
}

export interface GeoJsonFeatureProps {
  neighbourhood: string;
  neighbourhood_group: string;
  location: string;
}

export interface GeoJsonFeature {
  type: 'Feature';
  geometry: GeoJsonGeometry;
  properties: GeoJsonFeatureProps,
}

export interface GeoJsonData {
  type: 'FeatureCollection';
  features: GeoJsonFeature[];
}

export interface AirbnbMonthData {
  time: string;
  data: AirbnbListing[];
}

export type AirbnbAnnualData = AirbnbMonthData[];

export type AirbnbListing = AirbnbListingStringProps & AirbnbListingNumberProps;

export async function loadAirbnbData(): Promise<AirbnbData> {
  const airbnbRawData = await (await fetch('/airbnb-nyc-all.json')).json() as AirbnbAnnualData;
  return new AirbnbData(airbnbRawData);
}

export async function loadNYCGeoJson(): Promise<GeoJsonData> {
  const nycGeoJson = await (await fetch('/nyc.geojson.json')).json() as GeoJsonData;
  nycGeoJson.features.forEach((feature) => {
    const { neighbourhood, neighbourhood_group } = feature.properties;
    feature.properties.location = `${neighbourhood}, ${neighbourhood_group}, NY`;
  });
  return nycGeoJson;
}

function parseScores(ele: Element) {
  if (ele instanceof Image) {
    const alt = ele.alt;
    return parseInt(alt.split(' ')[0]);
  }
  return -1;
}

export interface WalkScoreData {
  scores: {
    walk: number,
    transit: number,
    bike: number,
    rank: number,
    residentsCount: number,
  },
  dtTraffic?: {
    drive: string,
    transit: string,
    bike: string,
    walk: string,
  },
}

export async function loadWalkScoreData(location: string) {
  const searchArr = location.split(',').map(d => d.trim());
  const [townName] = searchArr;

  const summaryRes = await fetch(`NY/New_York/${townName.replace(' ', '_')}`, { timeout: 500, redirect: 'manual' });
  if (summaryRes.ok) {
    const summaryHTML = await summaryRes.text();

    const reader = document.implementation.createHTMLDocument('reader');
    const readerHTML = reader.createElement('html');
    readerHTML.innerHTML = summaryHTML;

    const walkScoreEle = readerHTML.querySelector('#hood-badges > div > div > div > div > div > img:nth-child(1)')!;
    const walkScore = parseScores(walkScoreEle);

    const transitScoreEle = readerHTML.querySelector('#hood-badges > div > div > div > div > div > img:nth-child(2)')!;
    const transitScore = parseScores(transitScoreEle);

    const bikeScoreEle = readerHTML.querySelector('#hood-badges > div > div > div > div > div > img:nth-child(3)')!;
    const bikeScore = parseScores(bikeScoreEle);

    const residentEle = readerHTML.querySelector('#summary > div > div:nth-child(2) > p:nth-child(2)')!;
    const [, scoreRank, residentsCount] = /is the ([0-9]+).* with ([0-9,]+) residents/gmi.exec(residentEle.textContent!);

    const result: WalkScoreData = {
      scores: {
        walk: walkScore,
        transit: transitScore,
        bike: bikeScore,
        rank: parseInt(scoreRank),
        residentsCount: parseInt(residentsCount.replace(/,/g, '')),
      },
    };

    const aptElem = readerHTML.querySelector('.rentals-sidebar-item');
    if (aptElem) {
      const linkElem = aptElem.querySelector('a')!;
      const detailUrl = linkElem.href;

      const detailRes = await fetch(detailUrl, { timeout: 500, redirect: 'manual' });
      if (detailRes.ok) {
        const detailHTML = await detailRes.text();
        readerHTML.innerHTML = detailHTML;

        const driveTime = readerHTML.querySelector('.mode-drive')!.nextSibling!.textContent!.trim();
        const transitTime = readerHTML.querySelector('.mode-transit')!.nextSibling!.textContent!.trim();
        const bikeTime = readerHTML.querySelector('.mode-bike')!.nextSibling!.textContent!.trim();
        const walkTime = readerHTML.querySelector('.mode-walk')!.nextSibling!.textContent!.trim();

        result.dtTraffic = {
          drive: driveTime,
          transit: transitTime,
          bike: bikeTime,
          walk: walkTime,
        };
      }
    }

    return result;
  }

  return null;
}

(window as any)['load'] = loadWalkScoreData;
