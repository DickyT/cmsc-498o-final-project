import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon, FontAwesomeIconProps } from '@fortawesome/react-fontawesome';

const ButtonContainer = styled.div`
  display: inline-flex;
  cursor: pointer;
  padding: 2px;
`;

const HoverOutline = styled.div<{ size: number }>`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: ${({ size }) => size}px;
  height: ${({ size }) => size}px;
  border-radius: 100%;
  transition: background-color 0.2s;
  cursor: pointer;
  
  &:hover {
    background-color: #E0E0E0;
  }

  &:active {
    background-color: #BDBDBD;
  }
`;

interface IIconButtonProps extends FontAwesomeIconProps {
  buttonSize?: number;
}

const IconButton: React.FC<IIconButtonProps> = ({ onClick, buttonSize = 35, ...props }) => {
  return (
    <ButtonContainer>
      <HoverOutline onClick={onClick as any} size={buttonSize}>
        <FontAwesomeIcon {...props} />
      </HoverOutline>
    </ButtonContainer>
  );
};

export default IconButton;
