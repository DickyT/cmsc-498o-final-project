import moment from 'moment';
import _ from 'lodash';

import { AirbnbAnnualData, AirbnbListing } from './utils';

type FilterFunc = (data: AirbnbListing) => boolean;

type ReviewsData = { type: string, value: number, size: number };
type ReviewsDataAll = { time: moment.Moment, data: ReviewsData[] }[];

export default class AirbnbData {
  readonly data: AirbnbAnnualData;
  private currentTime = '';

  constructor(data: AirbnbAnnualData) {
    this.data = data;
    if (data[0]) {
      this.currentTime = data[0].time;
    } else {
      throw new Error('Data parsing failed');
    }
  }

  setCurrentTime(time: string) {
    if (this.timeList.indexOf(time) !== -1) {
      this.currentTime = time;
    }
  }

  getCurrentTime() {
    return this.currentTime;
  }

  get timeList(): string[] {
    return this.data.map(d => d.time);
  }

  get currentData() {
    return this.data.find(d => d.time === this.currentTime)!.data;
  }

  groupBy<T>(key: keyof AirbnbListing, filter?: FilterFunc, time?: string): Map<T, AirbnbListing[]> {
    const tempMap: Map<T, AirbnbListing[]> = new Map();

    const data = time ? this.data.find(d => d.time === time)!.data : this.currentData;
    data.forEach((record) => {
      if (!filter || filter(record)) {
        const groupByValue = record[key] as any;
        if (!tempMap.has(groupByValue)) {
          tempMap.set(groupByValue, []);
        }
        tempMap.get(groupByValue)?.push(record);
      }
    });

    return tempMap;
  }

  groupByMean<T>(key: keyof AirbnbListing, propName: keyof AirbnbListing, filter?: FilterFunc): Map<T, number> {
    const resultMap: Map<T, number> = new Map();
    const groupByData = this.groupBy<T>(key, filter);

    for (const [locationName, dataList] of groupByData.entries()) {
      const ratingSum = dataList.reduce((acc, curr) => {
        return acc + (curr[propName] as number);
      }, 0);
      const avgRating = ratingSum / dataList.length;
      resultMap.set(locationName, avgRating);
    }

    return resultMap;
  }

  groupByCount<T>(key: keyof AirbnbListing, filter?: FilterFunc): Map<T, number> {
    const resultMap: Map<T, number> = new Map();
    const groupByData = this.groupBy<T>(key, filter);

    for (const [locationName, dataList] of groupByData.entries()) {
      resultMap.set(locationName, dataList.length);
    }

    return resultMap;
  }

  reviewsDataByRoomType(location: string): ReviewsDataAll {
    const reviewsData: ReviewsDataAll = [];

    this.timeList.forEach((time) => {
      const subList: ReviewsData[] = [];
      const groupByData = this.groupBy<string>('roomType', d => d.location === location, time);
      for (const [roomType, dataList] of groupByData.entries()) {
        const reviewsCount = _.sumBy(dataList, 'reviews');
        subList.push({ type: roomType, value: reviewsCount, size: dataList.length });
      }
      reviewsData.push({
        time: moment(time, 'YYYY-MM'),
        data: subList,
      })
    });

    return reviewsData;
  }
}
