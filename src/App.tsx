import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';

import 'antd/dist/antd.css';

import MapBrowser from './components/MapBrowser';
import { loadAirbnbData } from './utils/utils';
import AirbnbData from './utils/AirbnbData';

const GlobalStyle = createGlobalStyle`
  body {
    overflow: hidden;
  }
`;

const AppWindow = styled.div`
  width: 100vw;
  height: 100vh;
`;

export interface IAppContext {
  data: AirbnbData;
}

export const AppContext = React.createContext<IAppContext>(null as any);

function App() {
  const [airbnbData, setAirbnbData] = React.useState<AirbnbData | null>(null);

  React.useEffect(() => {
    // Load airbnb data
    loadAirbnbData().then(setAirbnbData);
  }, []);

  let content: JSX.Element | null = null;
  if (airbnbData) {
    content = (
      <AppContext.Provider value={{ data: airbnbData }}>
        <MapBrowser />
      </AppContext.Provider>
    );
  }

  return (
    <>
      <GlobalStyle />
      <AppWindow>
        {content}
      </AppWindow>
    </>
  );
}

export default App;
